#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = "Comité pour l'annulation du sommet Afrique France 2021 à Montpellier"
SITENAME = 'Sommet France-Afrique à Montpellier ? C’est non !'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Paris'

THEME = 'theme/brutalist'
THEME_STATIC_DIR = 'theme'
OUTPUT_PATH = 'output'

DEFAULT_LANG = 'fr'

# Social media
FACEBOOK = 'https://www.facebook.com/Sommet-France-Afrique-%C3%A0-Montpellier-cest-non-100155935542941'
ATTRIBUTION = True

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('Python.org', 'http://python.org/'),
         ('Jinja2', 'http://jinja.pocoo.org/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

# Custom Jinja2 filter to get the Index page from Markdown
# From https://siongui.github.io/2016/02/19/pelican-generate-index-html-by-rst-or-md/
def hidden_pages_get_page_with_slug_index(hidden_pages):
  for page in hidden_pages:
      if page.slug == "index":
          return page


JINJA_FILTERS = {
    "hidden_pages_get_page_with_slug_index": hidden_pages_get_page_with_slug_index,
}

