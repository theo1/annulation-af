Title: Yopougon, l'Hisoshima ivoirien
Authors: Clotilde Ohouochi
Date: 2021/09/29
Category: Communications
Slug: clotilde-ohouochi-yopougon-hiroshima-ivoirien

_Clotilde Ohouotchi est ancienne ministre de la Solidarité, de la Santé, de la Sécurité sociale et des Handicapés de Côte d'Ivoire._

_Extrait de son ouvrage autobiographique [«&nbsp;Côte d'Ivoire: On ira jusqu'au bout !&nbsp;»](https://www.editions-harmattan.fr/livre-cote_d_ivoire_on_ira_jusqu_au_bout_clotilde_ohouochi-9782343006222-40498.html) (Paris, L'harmattan, 2013), portant sur la bataille d'Abidjan (avril-mai 2011), Chapitre 3 «&nbsp;Yopougon, l'Hiroshima ivoirien&nbsp;»_

«&nbsp;Si la vague t'entraîne, n'aie pas peur de la mort. Il n'a pas dit que tu coulerais. Il a dit: allons de l'autre bord&nbsp;». De la maison voisine, ce cantique s'élève, bouleversant, poignant. Entre deux détonations. Ici, comme ailleurs, dans de nombreuses familles prises au piège des combats, les prières se font ardentes. L'on n'implore plus la faveur du Ciel pour avoir la vie sauve mais on recommande plutôt son âme à Dieu en célébrant, par anticipation, des funérailles collectives. La fin semble si proche, en effet, si palpable, inéluctable. La vieille dame à la faux rôde partout, se pourléchant les babines et exhalant son souffle glacial à chaque déflagration. Le bombardement de Yopougon, la commune-martyr, reprend son cours infernal après quelques heures d'une relative accalmie. Sans répit, avec hargne et obstination, les hélicoptères de la coalition internationale volant en rase-mottes larguent leurs lourdes charges meurtrières sur des «objectifs militaires» et autres «caches d'armes» dans la plus grande agglomération de la Côte d'Ivoire. Le ballet incessant et le vrombissement assourdissant de ces insectes géants provoquent des décharges d'adrénaline difficiles à contenir.

Dans la petite maison où je vis cloîtrée depuis une semaine, le scénario reste quasiment immuable. Chaque fois que s'annonce le concert terrifiant des machines de guerre, femmes et enfants, mus par l'instinct de survie, se glissent sous des abris de fortune censés leur offrir une protection contre le déluge des bombes. Angles de murs, coins de meubles et dessous de lits sont pris d'assaut dans un mouvement de panique indescriptible. Les hommes, eux, restent assis au salon, statufiés, le regard vague, murés dans un silence de tombe.

Ce jour-là, les combats sont d'une extrême âpreté comme en témoignent la fureur des armes et le rythme effréné des détonations. Ils semblent plus près aussi. Jusque-là, «notre» secteur, situé à la lisière ouest de Yopougon et partiellement épargné par les affrontements, représente un îlot de paix et de sérénité relatives vers lequel affluent des habitants d'autres quartiers fuyant la terreur et les atrocités dont sont victimes les partisans du président Gbagbo ou les supposés tels. Mais les hommes de Monsieur Ouattara, soutenus par LICORNE, l'ONUCI et les supplétifs des armées de certains pays de la sous-région, avancent inexorablement au sol. Les uns après les autres, les quartiers tombent sous leur coupole. Déjà, Andokoi, Gesco, Wassakara, Maroc, Koweït[^1]...ont basculé dans leurs redoutables escarcelles.

Ce jour-là, les bombardements font rage. L'effroi règne partout, en maître absolu. Soudain, une sourde explosion déchire l'air suivie d'un bruit de tôles froissées juste au-dessus de ma tête. Une sorte de pluie de grêles crible le toit. Sous la puissance du choc, les murs, pris d'une transe endiablée, vibrent dangereusement. Je retiens mon souffle et attends, résignée, l'éclat d'obus ou tout autre projectile meurtrier qui atterrira droit sur ma cachette. Sans défense, taraudée par une peur indicible, je ferme les yeux. N'ayant plus la force de prier, je serre nerveusement à m'en briser les phalanges le chapelet lumineux aux grains nacrés que m'a offert un ami évêque et dont je ne me sépare plus depuis le déclenchement de ces événements apocalyptiques.

Pendant ce temps, PUMA et MI24 vomissent sans relâche le feu de leurs entrailles gonflées de roquettes. Durant une éternité, le déchaînement de ces engins de mort atteint le point culminant de la violence, l'épicentre de la folie meurtrière. Puis, comme par enchantement, leur fracas va decrescendo et s'arrête totalement. Surprise d'être en vie, je tends une oreille fébrile: les monstres froids ont-ils achevé leur funeste mission du jour? Ont-ils regagné leurs bases, leur boulimie assassine assouvie? Un calme étrange, peu rassurant, flotte dans l'air. Bientôt, une porte claque. Des rires d'enfants éclatent, purs, tels du cristal, enthousiastes et insouciants. Des pleurs aussi mais vite réprimés par des hurlements d'exaspération intimant le silence. Un cliquetis métallique suivi d'un crissement strident sur le dallage laisse deviner aisément que les ménagères, profitant de la trêve, s'affairent autour des fourneaux pour offrir à leurs familles un peu de nourriture. L'unique repas de la journée. Je soupire: même dans cet univers chaotique, la vie reprend triomphalement ses droits. Chaque instant gagné représente une ultime victoire, une belle revanche prise sur la mort!

Je sors de la chambre et regagne le salon après avoir rendu grâces à mon Dieu qui vient de nous sauver, mes compagnons et moi, d'une mort garantie. Encore une fois. Mon «tuteur» et la dizaine de déplacés internes qui, comme moi, a trouvé refuge sous son toit, devisent à voix basse. Les visages sont graves mais on y lit un air de soulagement. «On l'a échappé belle mais pour combien de temps» semble être la question qui tenaille tous les esprits face aux incertitudes du lendemain. Je jette un coup d'œil autour de moi et lève une mine interrogative vers mon chauffeur et mon garde du corps. Ces deux militaires, dont la fidélité et le dévouement à mon endroit ne souffrent d'aucune restriction, m'annoncent qu'ils ont méthodiquement inspecté la maison et qu'ils n'ont rien observé de suspect.

− Mais et cet effroyable bruit de tôles froissées? demandé-je d'un ton neutre, refusant, pour rien au monde, de laisser transparaître ma peur. Je sais que la moindre manifestation de faiblesse face à mes collaborateurs et devant les autres réfugiés signifie une humiliation de plus. Je feins donc un sang-froid que, dans l'engrenage de violence qui est notre quotidien, je ne possède pas, en réalité.

− Je vais me renseigner, dit le major.

Je n'ai donc pas rêvé. Une onde tellurique a bel et bien secoué la maison.

− Pensez-vous que cela soit prudent?

− Ne vous inquiétez pas, madame le ministre.

Pour des raisons évidentes de sécurité, il se risque dehors, sans armes, camouflé dans un ensemble jean. Les agents des FDS qui ne se sont pas ralliés au «gouvernement du Golf» constituent l'une des principales cibles des hommes de Monsieur Ouattara qui écument tous les quartiers d'Abidjan. Aussi, depuis le début des hostilités, mes «éléments» rangent-ils prudemment uniformes et kalachnikovs devenus d'encombrants et dangereux indicateurs.

Après quelques instants d'une attente insupportable, le militaire est de retour. Son visage impassible ne laisse filtrer aucune émotion. Il me prie de regagner ma chambre et fait signe à son collègue de le suivre.

− Madame le ministre, commence-t-il, les nouvelles sont terribles. À quelques mètres d'ici des familles entières ont été décimées, leurs maisons éventrées par des fragments de bombes.

− C'est affreux! soufflé-je, contenant avec peine un frémissement de colère impuissante. Seigneur, jusqu'à quand resteras-tu sourd à nos supplications? repris-je à mon compte la complainte du psalmiste.

− On peut disposer, madame le ministre?

Je les congédie, son collègue et lui, d'un signe de tête.

Le modeste quatre pièces de mon «tuteur» s'incruste dans un ensemble d'habitations économiques construites en bandes. C'est le prototype-même des opérations promotionnelles qui fleurissent dans les quartiers populaires d'Abidjan. Réalisées dans le cadre des politiques immobilières, ces maisons sont souvent livrées à leurs acquéreurs à moitié achevées. Certains les laissent en l'état, faute de moyens financiers, d'autres engagent des travaux de finition et d'embellissement pour les rendre fonctionnelles et conviviales. La maison de mon «tuteur» s'inscrit dans le deuxième cas.

Cadre de l'administration communale, mon «tuteur» dont je préfère préserver l'anonymat (de même que celui de la plupart des personnages de l'ouvrage), est un solide gaillard à l'orée de la quarantaine, originaire du Sud de la Côte d'Ivoire. Pénétré des principes de l'hospitalité africaine, il m'a généreusement cédé sa chambre conjugale qui offre plus de commodités et partage avec ses autres hôtes la rigueur de la natte au salon. La chambre de ses enfants réfugiés au village avec leur mère est occupée par leurs grands-parents maternels chassés de leur quartier pour délit de patronyme (ils sont originaires de l'Ouest ivoirien). La troisième chambre est également envahie par des familles de déplacés. La plupart d'entre eux sont de vagues connaissances de mon «tuteur». D'autres lui sont totalement inconnus. Ils ont juste frappé à sa porte et, face à leur détresse, il leur a ouvert. Cette terrible épreuve a, avec le sentiment profond d'oppression, de persécution et de bannissement, créé un élan émotionnel de solidarité entre exclus.

Mon seul univers demeure l'espace restreint qui est mis à ma disposition. Je n'en sors absolument jamais en dehors de rarissimes apparitions au salon en vue de me dégourdir les jambes. Mots fléchés, lecture de la Bible, prières restent mes uniques activités lorsque les temps sont cléments, bien entendu. Je note aussi, régulièrement, sur tout ce qui me tombe sous la main, informations, idées, réflexions et témoignages relatifs à la terrible tragédie qui se déroule sous nos yeux.

Une discussion animée se tient à présent au salon. J'en saisis sans difficulté chaque mot à travers les minces cloisons de «mes appartements».

− Encore des morts inutiles! s'exclame mon tuteur, furieux. À chaque jour son lot de victimes et son cortège de deuils. Il y a quelques jours les bombes de la coalition ont occasionné de nombreux dégâts humains à Locodjro et à Gesco, aujourd'hui, c'est près d'ici qu'elles tuent.

− Mais qui va punir Sarkozy et ses alliés pour le meurtre de toutes ces personnes innocentes, victimes collatérales ou dûment ciblées au sein de la population civile ivoirienne? Interroge une voix empreinte d'une douloureuse fatalité.

− Moi, je suis convaincu que tôt ou tard, d'une manière ou d'une autre, Sarkozy et ses complices paieront pour leurs crimes. Dieu nous vengera.

− Oh! Arrête avec ton Dieu. Où se trouve-t-il pendant qu'on nous bombarde? Je suis très fâché avec tous ces pasteurs, messagers de faux espoirs, qui prêchent une délivrance miraculeuse du peuple ivoirien.

− Nietzsche dit à peu près ceci à propos de l'espoir, qu'il est le plus grand drame de l'humanité en ce sens qu'il prolonge la souffrance de l'homme. Il a pleinement raison lorsqu'on rapporte son assertion au problème ivoirien. En effet, depuis le déclenchement de la rébellion en septembre 2002, les Ivoiriens sont alternativement passés par des phases de grand espoir à des périodes de grand désespoir. La paix, comme un mirage, capricieuse et insaisissable, s'estompe quand on croit l'avoir à portée de main.

− On se souvient tous que l'intervention militaire décidée par la fameuse Communauté internationale a été motivée, selon elle, par la «tuerie» de six ou sept femmes d'Abobo. Pourquoi cette Communauté internationale si soucieuse des questions de protection des droits humains s'adonne-t-elle alors à une entreprise de destruction massive en Côte d'Ivoire? Pourquoi se fait-elle complice du massacre à grande échelle des habitants de Yopougon?

La charge émotionnelle transparaît à travers le flot de questions sans réponses, faisant vibrer les voix.

− Avec la crise ivoirienne, nous voyons que les valeurs cardinales qui fondent les nations civilisées sont purement et simplement foulées aux pieds par ceux mêmes qui les prônent.

− La survie de l'Occident passe par une recolonisation de l'Afrique et la mainmise sur ses ressources et cela se fera sans état d'âme. Ne croyons pas trop au droit international et aux principes de paix.

− Pour venger six femmes, on tue des milliers de personnes.

− Ne dis pas cela car une vie est une vie, elle n'a pas de prix. Le problème est qu'il n'y a eu aucune enquête sérieuse qui démontre qu'elles sont effectivement tombées sous les balles des FDS.

− Les gens sont allés trop loin. Je me demande comment on pourra recoudre le tissu social si durablement lacéré.

− Les frères, n'oubliez pas que les Blancs ont le chic de créer des bizarreries pour diviser et opposer les Africains les uns aux autres afin de les entraîner dans des conflits fratricides sanglants. Au Rwanda, c'était nez aquilin contre nez épaté aux narines dilatées.

Cette dernière réflexion déclenche un fou-rire général, communicatif, véritable crème émolliente sur nos cœurs endoloris. Le niveau et la qualité des interventions me surprennent agréablement. Je m'abstiens cependant de participer au débat. Non par mépris envers ces frères venus des sous quartiers populaires de Yopougon mais par pudeur et, surtout, parce que cette vie de reclus dans cet environnement de suspicion généralisée requiert prudence et discrétion absolues. La loi de l'omerta s'impose à moi comme consigne d'extrême précaution.

La nuit tombe déjà. L'obscurité enveloppe à présent la cité comme le désespoir qui se glisse dans mon cœur. Le chauffeur à qui incombent les charges d'intendance dépose devant moi un plat fumant. Il est suivi de la belle-mère de mon «tuteur» dévouée tout entière à mon service.

− Mange ma fille, conseille-t-elle, maternelle. Tu ne peux pas prendre tes médicaments l'estomac vide.

Je lui grimace un pauvre sourire et fournis un effort surhumain pour chipoter dans l'assiette dont la frugalité du contenu, digne d'un repas d'ascète, ne parvient guère à réveiller mes papilles gustatives. De toutes les façons, il y a belle lurette qu'aucun bol alimentaire ne franchit aisément le barrage hermétique de ma gorge nouée. J'avale machinalement mes cachets puis, allongée sur le lit, je navigue dans ma mémoire pour tenter de remonter le cours des événements qui m'ont contrainte à la clandestinité dans la fournaise ardente de Yopougon-la-martyre, autrefois, Yopougon-la-joie...

[^1]: Maroc, Koweït, sont des noms de quartiers de la commune de Yopougon.
