Title: Afrique et dette
Authors: Nicolas Sersiron
Date: 2021/10/08
ry: Communications
Slug: nicolas-sersiron

_Nicolas Sersiron est ancien président du CADTM France et auteur de «&nbsp;Dettes et extractivisme&nbsp;»._

### Doxa et dette

Tous les endettements créés par les gouvernements des états dits démocratiques sont destinés, - selon la doxa dominante défendue par les pays riches comme la France, ainsi que l’Agence Française de Développement, l’AFD, le FMI, la Banque Mondiale, etc - à accélérer le développement, la croissance économique et en conséquence la création d’emplois, in fine, le progrès social.

### Remboursements

Les pays africains ont de grandes difficultés à rembourser leurs emprunts et leurs dettes augmentent depuis des décennies. « En 2021, les remboursements prévus aux créanciers de la part de (tous les) pays appauvris s’élèvent à 350 milliards de dollars. C’est 58 fois ce que l’ONU demande pour éviter les famines. » R.Vivien.

### Corruption, services publics et ressources naturelles

Les budgets nationaux sont plombés par une corruption et le détournement des élites du Sud, organisée volontairement par les pays « dits » développés. Les dettes n’étant pas investies dans l’économie, les remboursements se font alors au détriment des peuples : santé, éducation et services publiques gratuits ne sont plus qu’un lointain souvenir, la privatisation s’amplifie et l’injustice croît. Les ressources naturelles exportées sont bradées, l’agriculture vivrière est mise à mal, les accaparements de terre ne cessent pas.

### Pauvreté Sud, multinationales

Le développement économique n’est pas au rendez-vous promis et le progrès social absent. Le nombre d’affamés et d’habitants incapables d’avoir une vie active par carence alimentaire se chiffre en milliards. Donc, pour le continent africain, la dette n’a pas produit le développement économique et les créations d’emplois promis par les pouvoirs locaux, les pays riches « aidant » et les institutions financières dominées par les pays du Nord. A l’inverse, elle a très fortement participé à l’enrichissement des banques, des multinationales des pays industriels et d’une bourgeoisie locale corrompue.

### OMD vs ODD

Face à ces obstacles, l’ONU a créé en 2 000, les OMD, les objectifs du millénaire pour le développement. Soutenus par un très grand nombre de pays, ils étaient destinés à éliminer la grande pauvreté avant 2015, « à aider, dans le monde entier, des hommes, des femmes et des enfants à vivre mieux ». Devant le fiasco de cette initiative, les mêmes acteurs ont décidé de transformer les OMD en ODD, objectifs pour un développement durable. Ces deux mots accolés s’opposent, leur sens est aussi obscur que le projet qu’ils sous-tendent. Surtout lorsque l’on comprend que les pays développés sont les pilleurs, ceux qui pratiquent l’extractivisme depuis plusieurs siècles, et que les pays « dits en développement » sont les pays pillés, encore plus aujourd’hui qu’hier. Les 17 objectifs qui devront être atteints en 2030 ont pour buts la justice sociale et la préservation de la planète. On aimerait tellement y croire. Ces
très vertueux objectifs ne seront pas plus atteints que ceux des OMD précédents, sauf renversement totale de la situation.

### Histoire Afrique

Le continent africain est depuis 5 siècles un réservoir de ressources humaines et naturelles. Malgré ces immenses pillages réalisés par les pays industrialisés, il reste encore des forêts, des minerais, du pétrole, des terres et des poissons à voler, des enfants, des femmes et des hommes à exploiter. On peut affirmer que, sans ces ressources ni l’Europe, qui la première a puisé dans les richesses humaines et naturelles de ce continent, ni les Etats-Unis et les autres pays   industrialisés dont la Chine, n’auraient pu atteindre le développement matériel et la puissance dont ils sont si fiers aujourd’hui.

### Commerce triangulaire

Le commerce triangulaire, Europe-Afrique-Amérique-Europe - qui a tellement enrichi l’Europe - n’a pu exister que par la mise en esclavage de plusieurs dizaines de millions d’africains à partir du 16 e . Extraits par la force de leur territoire ancestral, ils ont été contraints aux travaux forcés dans les mines et sur les terres américaines colonisées pendant plus de trois siècles. Par ailleurs on estime à 50 millions, a minima, le nombre de natifs des Amériques a avoir été exterminés par les conquérants blancs qui se sont emparés de leurs territoires, forêts, mines, fleuves, mers, montagnes au sud comme au Nord.

### Fin de l’esclavage, colonisation de l’Afrique, indépendances

C’est seulement au milieu du 19 e , que l’esclavage et le commerce triangulaire ont enfin pu être arrêtés grâce aux féroces guerres locales (victoire de Toussaint Louverture sur l’armée de Napoléon en 1804 à Haïti) et à la résistance de nombreuses femmes et hommes libres. Cependant, et simultanément, l’Europe entame alors la colonisation armée de l’Afrique. L’occupation durera un siècle. Pillages extractivistes, exactions multiples, assassinats et travaux forcés seront le quotidien de nombreux africaines et africains pendant cette période. Face aux luttes armées de résistance et à la prise de conscience des peuples occidentaux, les européens cesseront leurs occupations militaires progressivement après la seconde guerre mondiale. La capitulation des anglais en Inde en 1947 face à la résistance du peuple indien massé derrière Gandhi sera le premier signal de cette grande retraite.

### Installation d’un colonialisme masqué

Le pillage extractiviste qui a tant profité aux pays industrialisés ne s’arrêtera pas pour autant. Pas question de lâcher la proie. L’asservissement de ces pays par l’occupation armée sera remplacée par les dettes illégitimes et les démocraties d’apparence. Organisé par les pays industriels, le néo-colonialisme se réalisera sous la forme de deux actions principales : Les éliminations-assassinats de nombreux élus démocrates travaillant au bien être de leurs peuples et leur remplacement par des dirigeants corruptibles (Lumumba assassiné, Mobutu sera institué président de l’ex Congo Belge) d’une part. D’autre part, l’impossible remboursement de dettes illégitimes et les baisers de la mort du FMI et de la Banque Mondiale venus à leur « secours ». Ainsi, les dettes, très majoritairement détournées - ou investies dans des éléphants blancs (cathédrale de Yamoussoukro en Côte d’Ivoire) au profit des multinationales du Nord - n’apporteront aucun développement.

### Dettes illégitimes, levier de l’extractivisme

Par contre les dettes publiques seront remboursées par le travail des peuples. Pire encore, elles serviront de levier pour contraindre ces pays à exporter leurs ressources naturelles en brut et transformer leur agriculture vivrière en cultures destinées à l’export au nom de remboursements pourtant impossibles. Si vous ne pouvez pas payer, creusez ! Une dette doit être remboursée !

L’avènement de la société de consommation en Occident, puis en Asie, avec son besoin insatiable de ressources naturelles, volées ou très peu payées, explique pourquoi pauvreté et faim sont plus importantes aujourd’hui qu’il y a 60 ans en Afrique, au moment des indépendances.

### Eco-Macron, ALE

Pour ce qui est des pays francophones, le panorama de la Françafrique est éclairant. Le Franc CFA, très contesté, a subi une fausse transformation. Remplacé
progressivement par l’ECO-Macron, il ne supprimera pas la vassalisation par la monnaie des états utilisant le CFA-ECO. Les accords de libre-échange (Ale et APE) entre un partenaire 100 fois plus puissant, très défavorables aux pays africains, sont renforcés.

### Aide publique inversée, dette, paradis fiscaux

L’aide publique au développement gérée par l’AFD est essentiellement une diplomatie des matières premières et une autoroute pour les entreprises françaises au détriment des sociétés locales. Les multiples corruptions des multinationales et des pays du Nord ne cessent pas. Les dettes illégitimes, odieuses, illégales ou insoutenables ne sont pas abolies malgré les immenses difficultés de ces pays à effectuer les remboursements et les coupes drastiques dans leurs dépenses publiques. L’évasion financière grâce aux Paradis fiscaux est considérable.

### Démocratie vs asservissement, ODD hypocrites

On ne parle au Nord que de démocratie et de liberté alors que le but n’est autre que l’asservissement de ces pays, peuples et gouvernants, pour profiter de leurs richesses à bon compte. Les agissements des services secrets du Nord et les autres manipulations politiques françaises empêchent les pouvoirs démocratiques de s’exercer. Les autres pays industrialisés pour lesquels les ressources naturelles africaines et humaines sont indispensables au maintien de leur suprématie ont plus ou moins les mêmes pratiques. Ce sont les maux essentiels qui rendent les beaux discours des ODD hypocrites.

### Faux espoirs ODD et matérialisme

Mais tout est fait pour que les citoyens du Nord croient possible la concrétisation de ces 17 objectifs pleins d’espoir. En France tous les médias appartiennent soit au gouvernement qui ment, soit à une dizaine de milliardaires qui mentent tout autant. Ces derniers ont tout intérêt à ce que les citoyens français ne comprennent pas ce qui se passe en Afrique. Il est indispensable qu’ils croient que la pauvreté des populations des pays « dits en développement » vient de leur manque de travail, d’inadaptation au monde moderne voire de corruption endémique : oubliant au passage qui sont les corrupteurs. Les français peuvent ainsi continuer à vivre dans l’hyper confort matérielet le gâchis consumériste, certes de façon inégalitaire, sans se sentir ni responsables ni coupables.

### 2189 Milliardaires

Aujourd’hui les milliardaires dirigent le monde. En 2020, ils ont vu leur fortune dépasser pour la première fois les 10 000 milliards de dollars pendant que l’économie mondiale s’écroulait sous l’effet des restrictions liées au Covid. La puissance financière des multinationales, dont ils sont les maîtres, leur permet de corrompre les élus et de dicter les lois par un lobbying intense pour installer la politique qui les enrichit au détriment du reste de l’humanité et de la nature. Cette perversion quasi totale du système démocratique doit être cachée aux peuples.

### Mensonges et médias
La propriété des médias leur permet de faire passer de fausses informations mêlées à quelques bribes de vérité, de ne pas parler de ce qui révélerait leurs vols, corruptions, dégâts écologiques, et ainsi de créer suffisamment de confusion dans la tête de ceux qui les croient, pour ne pas être désignés comme les grands responsables de la catastrophe. Si la moitié de la population, voire moins, comprenait cela, ne pensez-vous pas qu’elle se lèverait pour faire changer les choses ? Alors que les pauvres meurent de faim en Afrique et ailleurs, que la nature et le climat se dégradent rapidement, ces maîtres du monde, qui n’ont jamais été aussi riches, n’apparaissent pas encore aux yeux du plus grand nombre comme les premiers responsables de ces crimes.

### Gilets jaunes et citoyens tirés au sort

150 citoyens, tirés au sort, ont été réunis dans la Convention Citoyenne pour le Climat, la C.C.C., en France en octobre 2019 pour déterminer des mesures pour lutter contre le changement climatique tout en respectant la justice sociale. Précisément, un après les premières manifestations des Gilets Jaunes qui ébranlèrent si fortement le pouvoir français. Ces derniers refusaient de subir les augmentations du prix des carburants faites au nom de la préservation du climat. En effet, ils ne gagnaient pas suffisamment d’argent avec leur travail pour payer cette taxe carbone supplémentaire. Aller travailler avec leur véhicule, là où n’existaient pas de transport en commun devenait impossible. A partir d’une lutte face à l’urgence, ils ont collectivement pris conscience de l’injustice sociale globale à laquelle ils étaient assignés depuis longtemps.

### Promesses non tenues

Les citoyens lambda de la C.C.C., majoritairement ignorants des grands enjeux de société, se sont mis au travail avec ardeur. Sur les 149 propositions qu’ils ont émises, de façon fort judicieuse, après 6 mois de travail, le gouvernement Macron-des-riches n’en a retenu que 28, contrairement à sa promesse initiale de toutes les appliquer. Pourquoi ? Parce qu’elles auraient remises en cause tout l’édifice pyramidal de la financiarisation de l’économie à leurs profits ainsi que la privatisation des services publics.

### Lobbying, vol, bénéfices des mutinationales

Est-il acceptable que les grandes multinationales de l’agroalimentaire comme Nestlé revendent l’eau puisée gratuitement dans les sources communes de régions comme Vittel en France ou ailleurs avec d’immenses bénéfices. Cela alors que les populations n’ont plus accès à l’eau de leur propre sous-sol ? Qu’en Inde les puits vidés par Coca Cola soient remplis par les eaux polluées de la fabrication et salissent la nappe phréatique sur des dizaines de km. Que Texaco-Chevron ait pu polluer délibérément la forêt amazonienne de l’Équateur sur des milliers d’hectares et que, condamné à payer 9 milliards de dollars, cette entreprise n’ait encore rien versé 30 ans après les faits. Est-il tolérable que les milices, soudoyées indirectement par les multinationales, fassent régner la terreur au Kivu pour que le cobalt soit extrait par des enfants esclaves avec une simple pioche dans des boyaux mal étayés qui s’écroulent trop souvent ? Cela au profit du Rwanda voisin, exportateur complice, mais avant tout pour le bénéfice de l’industrie mondiale de la high tech.

### Endettement de la sécurité sociale

Est-il acceptable que la sécurité sociale française versent des dizaines de millions d’euros pour des tests Covid controversés et des injections expérimentales au prix fort à des multinationales privées qui engrangent des centaines de milliards de bénéfices ? Le Covid va mettre à genoux financièrement le système de santé publique français. Surendetter volontairement la sécurité sociale n’est-il pas le moyen trouvé par le gouvernement pour le privatiser encore plus vite au profit des mutuelles privées et des cliniques ?

### OMS

Est-il acceptable que l’OMS qui, face à une pandémie mondiale, informe sur les moyens d’y faire face, soit de fait aux mains d’entreprises privés ? Depuis quelquesannées ce sont les grandes entreprises du médicament, les fondations de santé (Gavi, Bill et Melinda Gates fondation, Rockfeller fondation, etc) et les banques qui financent très majoritairement cette institution internationale. Ce sont donc elles qui la dirigent de fait, évidemment pour leur bénéfice d’abord, avant celui des peuples ! Et que penser de cette aide qu’il faudrait donner aux pays africains car ils n’ont pas les moyens financiers de payer ces vaxxins alors que les populations ne sont pas ou très peu malades ? L’État indien de l’Uttar Pradesh, 250 millions d’habitants, a soigné la population avec de l’Ivermectine dès juin 2021. Après une flambée de l’épidémie en mai, il n’y avait quasiment plus de morts ni de cas en juillet.[^1]

## Reconquérir la souveraineté

### Autoritarisme et guerre sanitaire

La justice climatique, environnementale et sociale est au plus bas pour les africains, elles est en très forte régression en France et dans le monde dit développé, les inégalités sont croissantes pour tous les peuples de la planète. La liberté est très gravement remise cause en Chine, ailleurs l’autoritarisme se développe, la démocratie s’amenuise, la pauvreté progresse. Une guerre politico-sanitaire a été déclarée par les pouvoirs politiques sur toute la planète, l’économie mondiale a été mise en berne. Pendant que les petits entrepreneurs disparaissent, les valeurs boursières augmentent, les bénéfices des grands groupes accélèrent. C’est la guerre !

### Catastrophes annoncées et extensions du colonialisme

Notre monde arrive à un point de non retour climatique et environnemental. Le patriarcat - dont le sous-ensemble est nommé capitalisme - nous entraîne vers
d’inimaginables destructions. Les deux dernières guerres mondiales, parties d’Europe, ont dévasté le monde et tué 50 millions de personnes. Que dire des
immenses catastrophes environnementales et humaines qui s’annoncent ? Où seront-elles rangées dans l’histoire de l’humanité ? Depuis des décennies des hommes et des femmes de tous les pays ont lutté contre le colonialisme et son avatar le néo-colonialisme dans les pays du Sud. Sans succès notable. Cette troisième guerre mondiale, n’est-elle pas le fait de sociopathes milliardaires, qui tentent d’étendre cet asservissement colonial aux peuples des pays dits développés ?

### Autonomie alimentaire, énergétique, souverainetés

Quelles erreurs avons-nous commises ? Au moment de la dernière guerre mondiale une grande partie des peuples d’Europe avaient une forme d’autonomie alimentaire et économique. Les grandes usines taylorisés étaient encore peu répandues. En France, le quart de la population vivaient de la terre et nourrissait avec des produits locaux les autres français. Hors les grandes villes, chacun cultivait un lopin de terre. Aujourd’hui, au nom de ce que tout le monde a cru être le progrès, le confort matériel bâti sur l’extractivisme et le pillage, nous a fait perdre cette autonomie, base de toute souveraineté ? Certes la souveraineté politique n’était pas toujours au rendez-vous sinon les deux guerres mondiales n’auraient pas eu lieu. Mais aujourd’hui quelles souverainetés nous reste-il ?

Si demain une nouvelle pandémie apparaît, ou qu’une gigantesque panne d’électricité fige les grandes villes et bloque internet comme en Chine en septembre
2021, que mangerons-nous, de quelle énergie disposerons-nous ? Dans les années 1960, 60 % de l’énergie électrique française était d’origine renouvelable et sans danger : les barrages hydrauliques. Les pollutions industrielles croissaient fortement, c’était le début du réchauffement climatique et des destructions  massives de la nature. Mais il était encore possible d’éviter les catastrophes.

### Croissance zéro

En 1972, le rapport au club de Rome, réalisé sous la direction de Meadows, appelait à la croissance zéro pour faire face à la catastrophe environnementale programmée. Paul Ehrlich, biologiste auteur de « Population bomb » en 1968, arguant que la croissance démographique serait responsable de tous les maux de la terre, proposait comme solution de laisser une bonne partie de l’humanité mourir de faim!!! Un eugéniste plus radical que ceux qui sévissaient dans les camps de concentration nazis.

### Sédentarisation, domination, nature, technique

Depuis l’avènement de l’agriculture et de la sédentarisation qui lui est intrinsèquement liée, l’homme n’a cessé de vouloir dominer la nature. Cette idée a
permis au patriarcat de se développer. L’hubris, l’esprit de domination et la compétition sont progressivement devenus les moteurs de cette nouvelle organisation
sociale. La société dite moderne vénère la technique comme étant le mantra du progrès et de la compétition. Oubliant qu’à chaque avancée correspond une nouvelle destruction de la terre-mère et une nouvelle aliénation pour l’être humain. On commence enfin à en mesurer les conséquences, mais rien ne change. 50 ans plus tard, la croissance économique appuyée par les progrès techniques a continué sur lemême rythme. Les tenants de cette stratégie mortifère  s’entêtent à penser toujours et encore que la technique sera capable de résoudre les problèmes dont elle est la cause !!!

## Dénouer les fils de notre esclavage

Nous pensons que **les solutions** ne pourront advenir aussi bien en Afrique, en France et dans le monde que par :

... une forte décroissance des **inégalités**,

... l’effacement rapide du **patriarcat** : recherche de puissance, avidité, écrasement du féminin, domination destructive de la nature au lieu d’un sage contrôle de ses propres penchants autodestructeurs.

... la recherche d’une véritable **justice** qui permettra à tous les humains de manger, se soigner, se déplacer librement, de vivre une existence digne.

... une **éducation** de qualité vers l’autonomie pour les enfants

... une **agriculture** relocalisée, pérenne et à dimension humaine qui respecte les cycles de l’eau et de la vie des sols, seule capable de nourrir l’humanité sur le long terme

... **l’abandon** du confort matériel individuel en tant que valeur suprême remplacée par l’entraide, la solidarité, le partage, la sobriété, l’échange, la culture, l’autonomie, l’élévation de l’esprit et la pratique de toutes les valeurs qui permettront aux humains de faire société sans s’autodétruire

... **une lutte pour l’avènement d’une forme de gouvernement qui soit au service de tous et toutes**.

[^1]: [L’état d’Uttar Pradesh, en Inde, annonce qu’il est exempt de COVID-19, prouvant l’efficacité de l’Ivermectine](https://www.profession-gendarme.com/letat-duttar-pradesh-en-inde-annonce-quil-est-exempt-de-covid-19-prouvant-lefficacite-de-livermectine/comment-page-1/)
