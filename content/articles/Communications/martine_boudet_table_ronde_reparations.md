Title: Introduction à la table ronde «&nbsp;Réparations nécessaires en Afrique&nbsp;»
Date: 2021/10/01
Author: Martine Boudet
Category: Communications
Slug: martine-boudet-intro-reparations

_Martine Boudet est coordinatrice de «&nbsp;Résistances africaines à la domination néocoloniale&nbsp;» (Le Croquant, 2021)._

Nous sommes très heureux d'accueillir Clotilde Ohouotchi. Historienne de formation, elle a été ministre de la Solidarité, de la Santé, de la Sécurité sociale et des Handicapés en Côte d'Ivoire, d'octobre 2000 à décembre 2005, puis ancienne conseillère spéciale près de la présidence de la République en charge de l'Assurance maladie (de 2006 à 2011).[^1] Cela sous la présidence du socialiste Laurent Gbagbo, qui, pour rappel, est le leader du FPI/Front populaire ivoirien.

A ce titre, madame Ohouotchi a vécu de près l'édification démocratique dans son pays et dans la région, ainsi que la crise politique qui a démarré avec le coup de force des rebelles du Nord avec l'aide d'alliés extérieurs (Burkina Faso, France) à partir du 19 septembre 2002, et qui a abouti à la partition du pays, et à une guerre civile de presque dix ans.

La ministre a publié deux ouvrages, _Côte d'Ivoire: On ira jusqu'au bout !_ (Paris, L'harmattan, 2013)[^2], qui traite sur le mode autobiographique de la destruction du système démocratique de son pays, et _L'assurance maladie universelle (AMU) en Côte d'Ivoire - Enjeux, pertinence et stratégie de mise en œuvre_ (Paris, L'harmattan, 2015).[^3] Avec cette présentation par l'éditeur de ce deuxième livre:

> _Véritable innovation en Afrique subsaharienne, principalement en matière de mécanisme d'extension de la couverture santé, l'Assurance Maladie Universelle devient l'un des grands chantiers de la politique sociale du gouvernement. (...) L'Etat doit, par conséquent, créer des conditions idoines pour se substituer aux mécanismes traditionnels d'aide et de soutien, en inventant une politique de solidarité nationale ambitieuse, audacieuse, moderne, et rationnelle. Cet essai participe avant tout de la volonté affirmée de montrer que l'Assurance Maladie Universelle est possible en Afrique. Les pays africains, et principalement la Côte d'Ivoire, ont la capacité de faire croître harmonieusement des mécanismes d'Assurance Maladie Universelle malgré leur relative pauvreté économique._

L'un des articles de Clotilde Ohouotchi, intitulé « L'AMU ou les leçons d'une expérience inachevée », précise:

> _Aux réunions statutaires de la Conférence interafricaine Prévoyance sociale (Cipres), les experts français faisaient, sournoisement, une contre-publicité de l'AMU pour empêcher les autres pays africains de suivre l'exemple ivoirien. Le but de cette « guerre » réside dans la volonté des responsables français de protéger les intérêts colossaux que génèrent les maisons d'assurance hexagonales établies en Côte d'Ivoire. La politique du tiers payant généralisé, couvert par les organismes de l'AMU, ne réservait en effet plus que la portion congrue de la couverture complémentaire à l'assurance privée. Mais les autorités françaises voulaient, aussi et surtout, continuer à avoir l'initiative de la décision pour tout ce qui concerne la vie de leurs anciennes colonies._[^4]

En Afrique francophone, la présidence Gbagbo a été considérée par certains observateurs comme le pilier principal du processus de démocratisation qui a été initié dans les années 90, à la faveur de la fin de la guerre froide (symbolisée par la chute du mur de Berlin). Le silence médiatique ou bien des désinformations ayant souvent prévalu depuis l'intronisation du régime de Alassane Ouattara en 2011, nous avons de nombreuses interrogations, que je soumets à votre appréciation :

-quelles ont été les conditions d'exercice des gouvernements de l'ère Gbagbo, les principaux mandats qui ont pu être mis en application (en matière de démocratie et de développement), en somme leur bilan politique?

- Votre bilan concernant la réforme du système de protection sociale nous intéresse particulièrement. Quel a été le suivi de l'AMU?

- A votre avis, quelles sont les différentes responsabilités du drame ivoirien ? Les responsabilités nationales (des différents camps) et internationales? Quelle est la responsabilité spécifique de la France, sous les diverses présidences françaises depuis 2002 ? Un rendez-vous a t'il été manqué avec le premier ministre de la cohabitation, Lionel Jospin du PS, qui déclarait «ne pas vouloir intervenir dans les affaires intérieures de la RCI» ? Quel bilan faire en particulier de l'opération Licorne pour laquelle le groupe parlementaire communiste a demandé vainement en 2011 et en 2012 (soit après la chute du gouvernement Gbagbo) une commission d'enquête parlementaire ? [^5]

- L'actualité Afrique-France et des Outremers a été marquée récemment par la reconnaissance, par le président Macron, des responsabilités de l'Etat français dans le génocide des Tutsis au Rwanda, des massacres et mauvais traitements de Harkis à la fin de la guerre d'Algérie, et des préjudices causés à la population de la Polynésie française par les essais nucléaires. Des réparations morales ou matérielles sont à l'étude. La reconnaissance du massacre de centaines d'Algériens proches du FLN, à Paris le 17 octobre 1961, est également programmée.

Pensez-vous que des excuses et des réparations devraient être à l'étude de la même manière, concernant les relations franco-ivoiriennes et Afrique-France? Si tel est le cas, sous quelles formes et dans quels domaines devraient-elles intervenir, à votre avis ?

[^1]: [https://news.abidjan.net/articles/381624/clotilde-ohouochi-et-lamu-lassurance-maladie-universelle-lamu-une-victoire-pour-la-cote-divoire](https://news.abidjan.net/articles/381624/clotilde-ohouochi-et-lamu-lassurance-maladie-universelle-lamu-une-victoire-pour-la-cote-divoire), [https://news.abidjan.net/articles/355535/politique-sociale-de-gbagbo-ohouochi-clotilde-lamu-est-un-devoir-de-solidarite](https://news.abidjan.net/articles/355535/politique-sociale-de-gbagbo-ohouochi-clotilde-lamu-est-un-devoir-de-solidarite)

[^2]: [https://www.editions-harmattan.fr/livre-cote_d_ivoire_on_ira_jusqu_au_bout_clotilde_ohouochi-9782343006222-40498.html](https://www.editions-harmattan.fr/livre-cote_d_ivoire_on_ira_jusqu_au_bout_clotilde_ohouochi-9782343006222-40498.html)

[^3]: [https://www.editions-harmattan.fr/livre-l_assurance_maladie_universelle_amu_en_cote_d_ivoire_enjeux_pertinence_et_strategie_de_mise_en_oeuvre_clotilde_ohouochi-9782343060224-46315.html](https://www.editions-harmattan.fr/livre-l_assurance_maladie_universelle_amu_en_cote_d_ivoire_enjeux_pertinence_et_strategie_de_mise_en_oeuvre_clotilde_ohouochi-9782343060224-46315.html) , [https://www.youtube.com/watch?v=IoIu_vPs5lQ](https://www.youtube.com/watch?v=IoIu_vPs5lQ) , [https://www.amel-humacoop.org/wp-content/uploads/2018/07/COLLOQUEassurancemaladie.pdf](https://www.amel-humacoop.org/wp-content/uploads/2018/07/COLLOQUEassurancemaladie.pdf)

[^4]: [https://revue-progressistes.org/2020/11/12/lamu-ou-les-lecons-dune-experience-inachevee-clothilde-ohouochi/](https://revue-progressistes.org/2020/11/12/lamu-ou-les-lecons-dune-experience-inachevee-clothilde-ohouochi/)

[^5]:[https://groupe-communiste.assemblee-nationale.fr/propositions/propositions-de-re%CC%81solution/article/pr-no-3647-visant-a-creer-une-commission-d-enquete-sur-le-role-de-la-force](https://groupe-communiste.assemblee-nationale.fr/propositions/propositions-de-re%CC%81solution/article/pr-no-3647-visant-a-creer-une-commission-d-enquete-sur-le-role-de-la-force) , [https://www.assemblee-nationale.fr/14/propositions/pion0131.asp](https://www.assemblee-nationale.fr/14/propositions/pion0131.asp)
