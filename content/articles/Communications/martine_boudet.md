Title: A propos du colloque «&nbsp;Non au sommet France Afrique&nbsp;»
Authors: Martine Boudet
Date: 2021/10/11
ry: Communications
Slug: martine-boudet

_Martine Boudet est coordinatrice de «&nbsp;Résistances africaines à la domination néocoloniale&nbsp;» (Le Croquant, 2021)._

Que l'équipe organisatrice de Montpellier, et en particulier Charlotte Géhin son animatrice, soit remerciée pour la remarquable préparation de cet événement, cela dans un climat rare de convivialité et d'efficacité conjuguées. C'est un exemple d'élaboration autogérée, un exemple à faire connaître dans le contexte que nous connaissons bien de raréfaction des forces militantes.

Il s'agit d'un évènement international qui réunit en ce jour trois anciens ministres de pays d'Afrique de l'Ouest, Clotilde Ohouotchi (ministre de la Santé de Côte d'Ivoire), Kako Nubukpo (ministre de la Prospective du Togo) et Aminata Dramane Traoré (ministre de la Culture du Mali), aux côtés d'expert.es des problématiques néo et postcoloniales. A ce titre, et grâce également aux autres événements du contre-sommet[^1], le sommet officiel n'a pas le monopole et l'honneur (seul) des « expertises » et des « préconisations» de qualité. L'avenir dira si la médiation d'intellectuel.les tel.les Achille Mbembe pour « réinventer la relation Afrique France » peut jouer un rôle pour réduire les clivages abyssaux existant entre ces deux mondes[^2].

L'équipe de l'ouvrage _Résistances africaines à la domination néocoloniale,_ publié aux éditions du Croquant en mars dernier, est reconnaissante de la part belle faite à notre publication[^3]. Six auteur.es participent à ce colloque, en présentiel ou à distance. Nous partageons les un.es et les autres l'idée que des évolutions à l'échelle des relations Afrique France ne pourront se faire valablement et durablement sans des élaborations mixtes, qui fassent intervenir des spécialistes du Sud et du Nord, des coopérations Sud-Sud, du panafricanisme et de l'altermondialisme. Gus Massiah, Saïd Bouamama, Jacques Berthelot, entre autres auteur.es présent.es ce jour, pourront compléter le propos à ce sujet.

La photo de couverture du livre, qui est reprise sur l'affiche et les visuels du colloque, porte sur un groupe de femmes agricultrices du Nord Mali. C'est à titre d'exemple de populations qui sont étrangères aux mécanismes de domination et qui promeuvent des formes de coopération autonome, qui restent à faire valoir.

L'objectif qui est partagé ici, entre nos différentes équipes, est bien sûr de contribuer à desserrer l'étau d'une françafrique, d'un système néocolonial unique en son genre, qui asphyxie toujours davantage les peuples du Sud, sans apporter pour autant au peuple français le bien-être moral qu'il serait en droit d'attendre d'une coopération mutuellement profitable, et équitable.  L'objectif est de faire sortir les relations Afrique-France des oubliettes, et les citoyens français (y compris le mouvement social) du carcan nationaliste dans lequel ils sont enfermés.

Quoiqu\'occultée, cette question impacte en profondeur le système politique français. Le présidentialisme, en d'autres termes le centralisme autoritaire, qui fait exception à l'échelle de l'Union européenne, est directement relié à la françafrique. Deux éléments peuvent être pris en compte à cet égard : le degré de corruption d'élus et d'organisations qui sont des piliers de la 5ème République (voir la 2ème condamnation à de la prison ferme en 6 mois de l'ancien président Nicolas Sarkozy, dans l'attente du procès de l'affaire libyenne) et le degré de régression des droits démocratiques, qui impacte les habitants des quartiers populaires et les militants du mouvement social et écologique.

C'est à l'étude des mécanismes financiers, militaires, diplomatiques, linguistico-culturels... de ce verrou, et d'alternatives réalisables que s'est attelée l'équipe de _Résistances africaines._ C'est la médiatisation de cet enjeu géo-politique, encore trop méconnu, auprès des mouvements sociaux, de responsables politiques...que cette publication se donne pour objectif, entre autres publications de ce champ. Avec, en arrière-plan, le désamorçage de l'idéologie sécuritaire, source d'extrémisme nationaliste ainsi que d'autoritarisme et de xénophobie d'Etat. Le pari commencera à être gagné quand, en réponse aux thèses dangereuses sur « la guerre sans fin au terrorisme » et sur « le grand remplacement », seront mises sur la place publique les responsabilités de l'Etat français et de dictatures africaines complices, dans l'insécurité globale et dans ses suites en matière de migrations notamment, ce que subissent les peuples au quotidien.

Heureusement, notre tâche a été facilitée par l'actualité, surtout du côté du Sud, ponctuée qu'elle est de débats toujours plus intenses, sur ces deux problématiques en particulier: le départ des troupes françaises du Mali et du Sahel, et l'abolition du franc CFA. Entre autres événements, les changements d'exécutifs au Mali et en Guinée pour sortir du bourbier belliciste ou du cycle fatal des troisièmes mandats inconstitutionnels, font date, de même que les Etats généraux de la monnaie, dirigés par Kako Nubukpo en mai dernier à l'université de Lomé.

Notre livre s'est fait l'écho de ces campagnes d'opinion pour « une deuxième indépendance ». Le colloque d'aujourd'hui apportera, sans aucun doute, une pierre significative à ce débat.

**Bons débats et fructueux travaux !**

[^1]: [https://survie.org/IMG/pdf/flyer_contre_sommet_afrique_france_vvf-1.pdf](https://survie.org/IMG/pdf/flyer_contre_sommet_afrique_france_vvf-1.pdf)
[^2]: [https://sommetafriquefrance.org](https://sommetafriquefrance.org)
[^3]: [https://editions-croquant.org/actualite-politique-et-sociale/710-resistances-africaines.html](https://editions-croquant.org/actualite-politique-et-sociale/710-resistances-africaines.html)
