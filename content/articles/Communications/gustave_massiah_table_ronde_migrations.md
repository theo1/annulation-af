Title: Présentation de la table ronde sur les migrations
Authors: Gustave Massiah
Date: 2021/10/04
ry: Communications
Slug: gustave-massiah-table-ronde-migrations

_Gustave Massiah est co auteur et membre du Secrétariat international du FSM (représentant du Cedetim)._

Nous avons la chance d’avoir à notre table ronde Saïd Bouamama qui introduira la discussion ainsi que des camarades de l’Association Marocaine des Droits Humains et des camarades du comité des sans-papiers du département.

Quelques brèves réflexions pour commencer.

L’Histoire des Migrations se confond avec l’Histoire de l’Humanité. Elle a commencé en Afrique à partir des migrations des Néanderthaliens et de l’Homo Sapiens. Les migrants ne sont pas des intrus ; ils sont partie prenante de l’histoire de chaque société.

Dans l’histoire longue des migrations, un changement important survient, entre le 17ème et le 18ème siècle, avec le passage de l’Etat-Empire à l’Etat-Nation. Les Etats-nations n’ont pas existé de tous temps et ne sont pas une forme éternelle. L’identité nationale est d’invention récente, comme le disent si bien Edouard Glissant et Patrick Chamoiseau, chaque individu a des identités multiples ; il est réducteur et faux de vouloir le rabattre à une seule identité.

Dans la période actuelle, marquée par la mondialisation capitaliste, les migrations prennent trois formes. Les migrations économiques caractérisées par la différence des situations qu’on peut définir pour simplifier par l’impérialisme et le néocolonialisme. Comme l’exprimait très bien Alfred Sauvy dès 1950, « si les richesses sont au Nord et que les hommes sont au Sud, les hommes iront là où sont les richesses et vous ne pourrez rien faire pour les en empêcher ». Les migrations politiques résultent des guerres et des conflits et se traduisent par des déplacements de réfugiés. Les migrations environnementales qui commencent vont bouleverser les équilibres de la population mondiale.

Le droit international définit les grandes lignes des politiques migratoires. Il met en avant six principes de base : la dignité ; les droits des migrants ; la lutte contre le racisme ; la redéfinition du développement ; la liberté de circulation ; le droit international. La dignité est le fondement de toutes les propositions. Les migrants doivent être reconnus comme acteurs de la transformation des sociétés de départ et d’accueil et comme acteurs de la transformation du monde. Le respect des droits des migrants s’inscrit dans le cadre du respect des droits de tous. Le droit des étrangers doit être fondé sur l’égalité des droits et non sur l’ordre public. Il commence par la régularisation des sans-papiers Il met en avant le droit de vivre et travailler dans son pays et aussi le droit de libre circulation et d’installation. Il propose de reconnaître la citoyenneté de résidence

On prétend que la bataille idéologique s’organise autour de la question des migrations. C’est une instrumentalisation médiatique. La bataille pour l’hégémonie culturelle porte d’abord sur l’égalité. Les migrations sont utilisées mais partagent toujours autant les sociétés. Il y a autant d’appel à la haine que de manifestations de solidarité. Depuis quatre ans, les sondages annuels indiquent que 60% des sondés sont pour la citoyenneté de résidence et la participation des résidents étrangers aux élections locales. Et quand on les interroge sur leurs sujets d’inquiétude, les français mettent en tête le pouvoir d’achat et l’écologie, l’islam arrive en dixième position et l’immigration en treizième position.
