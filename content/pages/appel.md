Title: L'appel
Authors: theo
Category: pages
Tags:
Slug: l-appel
Weight: 1

_Signez cet appel sur [la pétition en ligne](https://www.change.org/p/les-citoyens-de-france-et-d-afrique-sommet-france-afrique-%C3%A0-montpellier-c-est-non)._

Habituellement localisé à Bordeaux, le sommet France-Afrique devrait avoir lieu cette année à Montpellier. Le sommet France-Afrique est l’expression de la défense des intérêts français dans les pays Africains. Rappelons-le, l'Afrique concentre en 2020 plus de 60% des individus extrêmement pauvres de la planète (personnes vivant avec moins de 1,90 dollar par jour) selon l'AFD [^1] et la banque mondiale. Elle enregistre 25 000 morts de faim par jour, selon la FAO [^2]. Et 25 millions d’emplois perdus en 2020 des suites de la crise.

Et pourtant…

L’Afrique est le continent qui est le plus riche en matières premières. Mais elle est soumise à un modèle productiviste uniquement basé sur l'exportation et profitant essentiellement à de grandes firmes étrangères, notamment européennes, et à une oligarchie locale corrompue. Un modèle imposé par les institutions financières où la France tient une place prédominante pour forcer les pays à récupérer des devises afin de rembourser leurs dettes, la France garantissant en outre le maintien de la convertibilité du FCFA, rebaptisé ECO, en euros. Mais cette monnaie trop forte nuit aux exportations africaines et encourage les importations de produits manufacturés et agricoles étrangers, ruinant les efforts de développement industriel et agricole de la Zone franc.

Pendant la crise sanitaire, le cours des matières premières a baissé, soulignant la dépendance économique des pays aux règles des marchés financiers. Les taux d'intérêt que réclament les créanciers privés, eux, s'envolent, tandis que les pays d'Afrique doivent s'endetter encore plus. Et les institutions financières n'y font rien, à part proposer de nouveaux chantages à la manière des plans d'ajustements structurels.

Au total, ce sont plus de 350 milliards de dollars de dette qui pèsent sur les épaules des pays africains.

Les conséquences sont connues : baisse des investissements dans les éc  les, les hôpitaux, les services publics. Accroissement de la pauvreté, de la malnutrition.
Et ce ne sont pas les APD [^3] ni même quelques actions d'ONG bienveillantes qui pourront remédier à la situation.

Quant au volet militaire, la France est engagée militairement dans deux opex rassemblant 5700 militaires en Mauritanie, Mali, Burkina Faso, Niger, Tchad.
Dans ces pays, on assiste à l'émergence de nouveaux conflits intérieurs, dont la corrélation ces deux dernières décennies peut être établie avec la présence militaire française. De plus, elle engendre ce que l'on appelle la « crise des migrants », avec son lot de citoyens africains fuyant la pauvreté et le chaos.

Cette logique de préservation des intérêts économiques et militaires Français a survécu au colonialisme jusqu’à aujourd’hui et fait la preuve de ses injustices sociale, économique et aussi environnementale.

Aussi, nous refusons que Montpellier devienne la vitrine du colonialisme qui ne dit pas son nom. L’argument des retombées économiques n'est pas une justification suffisante.

Nous demandons donc l’annulation du sommet à Montpellier et nous exigeons :

➔ **L’annulation des dettes odieuses et illégitimes des pays Africains**

➔ **Le retrait de toutes les troupes françaises du continent Africain**


## Premiers signataires (organisations et citoyens) :
### Citoyens :
_Charlotte Géhin, François Charles, Christian VERNET citoyen militant CGT, Amit Bellicha, Nicolas Sersiron, Jocelyne Ferrera-Sage, Patrick Saurin, Alban Desoutter, Thomas PEIXOTO, Pascal Franchet, ancien président du CADTM France, Martine Boudet, Jacques Berthelot, membre du Conseil scientifique d'Attac France , Jean-Pierre Aranega retraité Poste, Magali CROZIER infirmière, cheffe de file des Insoumis du Biterrois ; Gustave Massiah ; Nataléna Richard ; Sabine Raynaud ; Christophe Lopez ; Marie-Hélène Choukroun ; Geneviève COINDOZ ; Louis SALCEDO, syndicaliste (Carcassonne) ; Bruno JOULIA (Castelnaudary) ; Mafoud Berkane, membre LFI Narbonne ; Jacques Vieules, syndicaliste enseignant (Trèbes) ; Charles Burgaudeau, syndicaliste enseignant (Carcassonne) ; Patricia Sourrouille ; Jean-Marie Gillan ; Françoise Roche, Luis Ventura, Ragonnet Martine syndicaliste CGT Guerrin François syndicaliste CGT ; Annie Salsé syndicaliste CGT, Colette Rumeau militante PCF, Patrick Blin militant PCF ; Gérard Lorigny directeur d''école en retraite ; Claude Sifflet ; Aimé COUQUET, militant PCF ; David Garcia LFI ; Louis-Dominique AUCLAIR, Jean-Claude Rodriguez Maire de Brissac (34), Jean-Michel Carrez Insoumis de nature, Lucienne Bousquet militante du PCF, Thierry Canals Retraité - La Salvetat sur Agout, Maryse Launais Roquebrun, DIDER GADEA syndicaliste paysan "d'un monde où les éleveurs français crèvent dans leur ferme et sont surendettés ", Thierry Canals - Retraité - La Salvetat sur Agout ; Françoise MARILL ; Laurent Baibarac militant cgt ; Liliane DOTTA LFI ; Pierre QUARANTA LFI ; Bernard Deschamps, ancien député PCF du Gard, Georges Cèbe ancien maire de St –Pons de Thomières, Valérie Tabonnes LFI Béziers ; Bertille Vionnet ; Duflo Nicole LP27 ; Duflo Claude militant PCF ; Odile Pelet retraitée, Nadine BRIATTE, Retraitée de la fonction publique territoriale ; Christiane Forti, Abel Haidoux, insoumis, Pierre Roy libre penseur de la Loire, Marie-Christine MERCIER, Michel MARTIN syndicaliste dans la Haute Vallée de l'Aude, Henry GARINO, Gilet Jaune "Carcassonne en colère", Bernard COLIN, syndicaliste Villardebelle, Serge ALLEON , Membre de l'Association Nationale des Communistes, Martine Jouin, membre E ! et Mouvement de la Paix, Lucette Pagés Retraitée Education Nationale ; Alain CLARY, ancien Maire de Nîmes ; Vincent BOUGET, secrétaire départemental du Gard du PCF, membre du conseil national._

### Organisations :
_CNT34ESS, Fédération Libre Pensée 34, PCF 34 ; NPA 34 ; UCL Montpellier ; CEDETIM ; Solidaires 66, PCRF Aude ; les groupes d'actions Pic Saint Loup de la FI ; POI 11 ; Fédération Nationale Laïque des Associations des Amis des Monuments Pacifistes, Républicains et Anticléricaux ; Parti de Gauche / Alpes Maritimes (PG06)._

[^1]: Agence française de développement
[^2]: FAO : L'Organisation des Nations unies pour l'alimentation et l'agriculture
[^3]: APD : aide publique au développement.
