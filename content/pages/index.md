Title: Demandons l'annulation du sommet Afrique-France 2021 à Montpellier !
Authors:theo
Category:
Tags:
Status: hidden
Slug: index

## Communiqué

### Colloque pour l’annulation du sommet France Afrique : un incontestable succès internationaliste !

Ce samedi 2 octobre s’est tenu à Grabels le colloque du comité pour l’annulation du sommet France Afrique.

Durant une journée entière, pas moins de 15 intervenants se sont succédés devant une centaine de personnes, pour dénoncer chacun des aspects des relations de domination entre les compagnies et l’Etat francais d’un côté, et les Etats africains francophones, représentant des millions d’Africains de l’autre.

Ces discussions ont été placées sous le signe de la solidarité internationale, du combat commun pour la défense de tous les principes et lois qui permettent de vivre partout et ensemble: respect des souverainetés nationales, des acquis démocratiques et sociaux, des suffrages populaires, du droit à la terre et à vivre dans la paix civile,principe de non-ingérence pour régler les conflits soit disant "inter-ethniques", conquêtes sociales, services publics, défense des emplois, des droits tant sociaux que démocratiques, droit à la terre…

Notre comité tient à attirer l’attention sur la falsification qui s’opère autour de la présentation de ce sommet : quand côté cour, on vante les artistes, la musique, la culture, nous remarquons que côté jardin, la militante Aminata Traoré, qui a critiqué la présence militaire française au Mali, est interdite de séjour en France et dans l'espace Schengen.

La totalité du colloque sera accessible en ligne bientôt grâce à une équipe technique qui s’est dépensée sans compter.

Nous remercions l’ensemble des intervenants pour leur déplacement et leur participation, leur éclairage lumineux autour de ces questions. Un remerciement tout particulier à l’équipe de l'ouvrage "Résistances africaines à la domination néocoloniale" (Le Croquant, 2021). De même, un énorme merci au groupe LOKOLE34, qui est venu animer la soirée.

Enfin, nous remercions chaleureusement les organisations et personnes qui ont permis, par un apport financier, que cet évènement puisse avoir lieu.

Plus que jamais, nous estimons que ces sommets France Afrique, vitrines des opérations financières et militaires de la France, doivent être annulés. Rien ne pourra changer sans en passer par la rupture avec ces sommets, même relookés ou toilettés.

Le comité, qui a permis ce succès, appelle aussi à manifester le **9 octobre à Montpellier**, toujours sur les mêmes mots d’ordre :

* **Annulation des sommets**
* **Annulation des dettes**
* **Retrait des troupes Françaises.**

\- Le comité d’organisation
