Title: Programme du colloque
Authors:theo
Category: Colloque
Tags:
Slug: colloque
Weight: 2


Le colloque a eu lieu samedi 2 Octobre à Grabels.

Nous mettrons rapidement en ligne les captations des interventions.

## 09h - Introduction : Présentation de l'ouvrage collectif «&nbsp;Résistances africaines à la domination néocoloniale&nbsp;» (Le Croquant, 2021)
[Lire la présentation du livre](https://editions-croquant.org/actualite-politique-et-sociale/710-resistances-africaines.html).

Intervenant⋅es :

* **Martine Boudet**, coordinatrice du livre et membre du Conseil scientifique d'Attac France
* **Gus Massiah**, co auteur et membre du Secrétariat international du FSM (représentant du Cedetim)

![Couverture de l'ouvrage présenté]({static}../images/couv_resistances_africaines.jpg)

## 10h - Table ronde «&nbsp;Économie et finances&nbsp;»

Intervenant⋅es :

* **Kako Nubukpo**, coordonnateur des Etats Généraux de l'Eco, partie prenante du rapport commandé par E. Macron sur la situation en Afrique, commissaire du Togo à la Commission de l’Union économique et monétaire ouest-africaine (Uemoa), en distanciel,
* **Nicolas Sersiron**, ancien président du CADTM France, auteur de « Dettes et extractivisme », en présentiel ou distanciel,
* **Fadhel Kaboub**, professeur associé d’économie à l’université de Denison dans l’Ohio et président du think tank Global Institute for Sustainable Prosperity aux Etats-Unis, membre du FUIQP, en distanciel.

## 12h - Table ronde «&nbsp;Enjeux militaires&nbsp;»

Intervenant⋅es :

* **Claude Serfati**, économiste,Chercheur associé à l’Institut de Recherche économique et sociale et maître de conférence à l’Université Versailles-Saint-Quentin, auteur de "Le militaire : une histoire française", en présentiel.
* **Bernard Deschamps**, ancien député, président d’honneur de France-El Djazaïr, dernier ouvrage publié «&nbsp;Révolution, l’Algérie, Abdelaziz Bouteflika, le hirak&nbsp;», en présentiel.

## 14h30 - Table ronde «&nbsp;Migrations&nbsp;»

Intervenant⋅es :

* **Said Bouamama**, sociologue, animateur du blog Le blog de "Saïd Bouamama. révolution africaine", en présentiel.
* **Intervenants de l'Association Marocaine des Droits de l'Homme (AMDH)**, en présentiel.
* **Intervenants du Collectif Sans Papiers 34**, en présentiel.

## 16h30 - Table ronde «&nbsp;Réparations nécessaires en Afrique&nbsp;»

Intervenant⋅es :

* **Clotilde Ohouochi**, ancienne Ministre de la Solidarité, de la Santé et de la Sécurité sociale de la Côte d'Ivoire, en présentiel.
* **Martine Boudet**, membre d'Attac France, en présentiel.

## 19h - Concert

[Eric Mopero Lokole 34](https://www.facebook.com/LOKOLE34)
